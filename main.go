package main

import (
	"fmt"
	"net/http"
	"time"
)

func handlerIndex(w http.ResponseWriter, r *http.Request) {
	var message = "Welcome,Hello world!"
	fmt.Println(message)
	w.Write([]byte(message))
}

func main() {
	http.HandleFunc("/", handlerIndex)

	var address = "localhost:9000"
	var date = time.Now()
	fmt.Printf("server started at %s\n", address)
	fmt.Printf(date.Format("02-Januari-2006"))

	server := new(http.Server)
	server.Addr = address
	server.ReadTimeout = time.Second * 10
	server.WriteTimeout = time.Second * 10

	err := server.ListenAndServe()
	if err != nil {
		fmt.Println(err.Error())
	}
}
